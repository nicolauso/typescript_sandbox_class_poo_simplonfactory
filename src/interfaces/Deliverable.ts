export interface Deliverable {
    // Estime le délai de livraison en nombre de jours.
    estimateDeliveryTime(weight: number): number;
    // Calcule les frais de livraison.
    calculateShippingFee(weight: number): number;
}