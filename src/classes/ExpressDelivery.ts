import { Deliverable } from "../interfaces/Deliverable";

export class ExpressDelivery implements Deliverable {
      /**
     * Renvoie le temps estimé de livraison suivant le poids du colis
     * @param weight 
     * @returns 
     */
  estimateDeliveryTime(weight: number): number {
    let time = 0;
    if (weight <= 5) {
      time = 1;
    } else {
      time = 3;
    }
    return time;
  }
  
 /**
   * Renvoie la taxe de lviraison suivant le poids du colis
   * @param weight 
   * @returns 
   */
  calculateShippingFee(weight: number): number {
    let fee = 0;
    if (weight < 1) {
      fee = 8;
    } else if (weight >= 1 && weight <= 5) {
      fee = 14;
    } else if (weight > 8) {
      fee = 30;
    }
    return fee;
  }
}
