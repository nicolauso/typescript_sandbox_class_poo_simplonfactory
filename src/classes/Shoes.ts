// Sous-classes de Product : Clothing, Shoes.

import { ShoeSize } from "../utils/enum";
import { Dimensions, Product } from "./Product";

// Pour Clothing, ajouter la propriété size, de type ClothingSize.
// Pour Shoes, ajouter la propriété size, de type ShoeSize.
// Redéfinir les constructeurs pour chaque sous-classe afin de renseigner les valeurs des propriétés.
// Redéfinir displayDetails() pour chaque sous-classe pour inclure les nouvelles propriétés.

export class Shoes extends Product {
  // size: ShoeSize;
  // constructor(
  //     productID: number,
  //     name: string,
  //     weight: number,
  //     price: number,
  //     dimensions: Dimensions,
  //     size: ShoeSize
  //   ) {
  //     super(productID, name, weight, price, dimensions);
  //     this.size = size;
  //   }

  constructor(
    public productID: number,
    public name: string,
    public weight: number,
    public price: number,
    public dimensions: Dimensions,
    public size: ShoeSize
  ) {
    super(productID, name, weight, price, dimensions);
  }
  
  displayDetails(): string {
    return `${super.displayDetails()}, ${this.size}`;
  }
}
