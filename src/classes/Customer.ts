// Type Address : street (texte), city (texte), postalCode (texte), country (texte).

// Ajouter l'attribut address dans la classe Customer, de type Address | undefined.
// Inutile de modifier le constructeur de Customer.
// Ajouter la méthode displayAddress(): string : Retourne les informations de l'adresse au format : "Address: $street, $postalCode $city, $country" si l'adresse existe, ou "No address found" sinon.
// Modifier la méthode displayInfo() afin d'ajouter les informations de l'adresse.
export type Adress = {
  street: string;
  city: string;
  postalCode: string;
  country: string;
};

export class Customer {

    // Propriétés
    // customerID: number;
    // name: string;
    // email: string;
    // adress: Adress | undefined;

    // Constructeur
//   constructor(customerID: number, name: string, email: string, adress?: Adress) {
//     this.customerID = customerID;
//     this.name = name;
//     this.email = email;
//     //this.adress = adress;
//   }
  
  // Autre syntaxe pour déclarer un constructeur avec binding
  constructor(public customerID: number, public name: string, public email: string, public adress?: Adress) {}
  


  displayInfo(): string {
    return `Customer ID: ${this.customerID}, Name: ${this.name}, Email: ${this.email}. `;
  }

  displayAdress(): string {
    return this.adress
      ? `Adress: ${this.adress.street}, ${this.adress.postalCode}, ${this.adress.city}, ${this.adress.country}, `
      : `Aucune adresse renseignée`;
  }
}
