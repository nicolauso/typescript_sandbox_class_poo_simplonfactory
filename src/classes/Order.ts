import { Deliverable } from "../interfaces/Deliverable";
import { Customer } from "./Customer";
import { ExpressDelivery } from "./ExpressDelivery";
import { Product } from "./Product";

// - Ajouter une méthode estimateDeliveryTime(): Utilise la méthode estimateDeliveryTime de l'objet delivery pour estimer le délai de livraison de la commande.

export class Order {
  // Propriétés
  // orderId: number;
  // customer: Customer;
  // productsList: Product[];
  // orderDate: Date;
  delivery: Deliverable | undefined;

  // // Constructeur
  // constructor(
  //   orderId: number,
  //   customer: Customer,
  //   productsList: Product[],
  //   orderDate: Date
  // ) {
  //   // Binding
  //   (this.orderId = orderId),
  //     (this.customer = customer),
  //     (this.productsList = productsList),
  //     (this.orderDate = orderDate);
  // }

  constructor(
    public orderId: number,
    public customer: Customer,
    public productsList: Product[],
    public orderDate: Date
  ) {}

  /**
   * Permet d'assigner un service de livraison à la commande.
   * @param delivery
   */
  setDelivery(delivery: Deliverable) {
    this.delivery = delivery;
  }
  //   orderOfChocolat.setDelivery(new ExpressDelivery);

  /**
   * Utilise la méthode calculateShippingFee de l'objet delivery pour calculer les frais de livraison en fonction du poids total de la commande.
   *
   * @returns
   */
  calculateShippingCosts(): number | undefined {
    return this.delivery?.calculateShippingFee(this.calculateWeight());
  }

  //   - Ajouter une méthode estimateDeliveryTime(): Utilise la méthode estimateDeliveryTime de l'objet delivery pour estimer le délai de livraison de la commande.
  estimateDeliveryTime(): number | undefined {
    return this.delivery?.estimateDeliveryTime(this.calculateWeight());
  }

  /**
   * Ajoute un nouveau produit à la liste ProductsList
   * @param product
   */
  addProduct(product: Product): void {
    this.productsList.push(product);
  }
  removeProduct(productId: number): void {
    this.productsList = this.productsList.filter(
      (product) => productId != product.productID
    );
  }

  // Calcule le poids total de la commande.
  calculateWeight(): number {
    // let totalWeight = 0;
    // this.productsList.map((product) => totalWeight += product.weight  );
    // return totalWeight;
    return this.productsList.reduce(
      (totalProductWeight, product) => totalProductWeight + product.weight,
      0
    );
  }

  //   - calculateTotal(): Calcule le prix total de la commande.
  calculatePriceOrder(): number {
    // let totalWeight = 0;
    // this.productsList.map((product) => totalWeight += product.weight  );
    // return totalWeight;
    return this.productsList.reduce(
      (totalProductPrice, product) => totalProductPrice + product.price,
      0
    );
  }

  //   - displayOrder(): Affiche les détails de la commande : les informations de l'utilisateur, les informations de chaque produit et le total de la commande.
  displayOrder(): string {
    // Info customer
    let inforCustomer: string = this.customer.displayInfo();
    // Info adress customer
    let inforAdressCustomer: string = this.customer.displayAdress();
    //Info commande et chaque produit
    let infoProducts: string[] = this.productsList.map((product) =>
      product.displayDetails()
    );
    return `${inforCustomer}, ${inforAdressCustomer}, ${infoProducts}`;
  }
}
