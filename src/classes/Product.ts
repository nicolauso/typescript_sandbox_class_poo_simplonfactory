// Type Dimensions : length (numérique), width (numérique), height (numérique).

// Ajouter l'attribut dimensions à la classe Product, de type Dimensions.
// Modifier le constructeur de Product afin que la dimension soit renseignée.
// Modifier la méthode displayDetails() afin d'ajouter le détail des dimensions lors du retour de la méthode.

// Type Address : street (texte), city (texte), postalCode (texte), country (texte).
export class Product {
  // Propriétés
  //   productID: number;
  //   name: string;
  //   weight: number;
  //   price: number;
  //   dimensions: Dimensions;

  //   // Constructeur
  //   constructor(
  //     productID: number,
  //     name: string,
  //     weight: number,
  //     price: number,
  //     dimensions: Dimensions
  //   ) {
  //     this.productID = productID;
  //     this.name = name;
  //     this.weight = weight;
  //     this.price = price;
  //     this.dimensions = dimensions;
  //   }

  constructor(
    public productID: number,
    public name: string,
    public weight: number,
    public price: number,
    public dimensions: Dimensions
  ) {}

  displayDetails(): string {
    return `Product ID: ${this.productID}, Name: ${this.name}, Weight: ${this.weight}kg, Price: ${this.price}$, Dimensions : length :${this.dimensions.length}cm, width : ${this.dimensions.width}cm, height : ${this.dimensions.height}cm`;
  }
}

export type Dimensions = {
  length: number;
  width: number;
  height: number;
};
