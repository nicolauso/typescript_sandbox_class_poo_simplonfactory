// Sous-classes de Product : Clothing, Shoes.

import { Product } from "./Product";
import { ClothingSize } from "../utils/enum";
import { Dimensions } from "./Product";

export class Clothing extends Product {
  // Propriétés
  //   size: ClothingSize;
  //   //Constructeur
  //   constructor(
  //     productID: number,
  //     name: string,
  //     weight: number,
  //     price: number,
  //     dimensions: Dimensions,
  //     size: ClothingSize
  //   ) {
  //     super(productID, name, weight, price, dimensions);
  //     this.size = size;
  //   }

  // Autre syntaxe du constructeur avec binding
  constructor (
    public productID: number,
    public name: string,
    public weight: number,
    public price: number,
    public dimensions: Dimensions,
    public size: ClothingSize
  ) {
    super(productID, name, weight, price, dimensions);
  }

  displayDetails(): string {
    return `${super.displayDetails()}, ${this.size}`;
  }
}
