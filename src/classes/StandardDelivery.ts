import { Deliverable } from "../interfaces/Deliverable";

export class StandarDelivery implements Deliverable {
    /**
     * Renvoie le temps estimé de livraison suivant le poids du colis
     * @param weight 
     * @returns 
     */
  estimateDeliveryTime(weight: number): number {
    let time = 0;
    if (weight < 10) {
      time = 7;
    } else {
      time = 10;
    }
    return time;
  }

  /**
   * Renvoie la taxe de lviraison suivant le poids du colis
   * @param weight 
   * @returns 
   */
  calculateShippingFee(weight: number): number {
    let fee = 0;
    if (weight < 1) {
      fee = 5;
    } else if (weight >= 1 && weight <= 5) {
      fee = 10;
    } else if (weight > 8) {
      fee = 20;
    }
    return fee;
  }
}
