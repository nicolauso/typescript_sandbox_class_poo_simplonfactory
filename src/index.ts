import { Customer } from "./classes/Customer";
import { Product } from "./classes/Product";
import { ClothingSize, ShoeSize } from "./utils/enum";
import { Clothing } from "./classes/Clothing";
import { Order } from "./classes/Order";
import { ExpressDelivery } from "./classes/ExpressDelivery";
import { Shoes } from "./classes/Shoes";

const lola = new Customer(458, "lola", "lola@customer.fr");

lola.adress = {
  street: "baker street",
  city: "London",
  postalCode: "5285",
  country: "England",
};
console.log(lola.displayInfo());
console.log(lola.displayAdress());

const peanut = new Product(415, "peanut", 150, 14, {
  height: 45,
  width: 45,
  length: 847,
});
//console.log(peanut.displayDetails());

const culotte = new Clothing(
  4145,
  "culotte jolie",
  0.1,
  12,
  { height: 160, width: 100, length: 144 },
  ClothingSize.S
);
// console.log(culotte.displayDetails());

const crocs = new Shoes(
  4875,
  "Crocs",
  214,
  84,
  { height: 84, width: 422, length: 12 },
  ShoeSize.EU42
);

const panierFruits = new Product(4587, "panier_de_fruits", 45, 487, {
  height: 15,
  width: 13,
  length: 55,
});

const choucroute = new Product(87569, "choucroute", 14, 2, {
  height: 15,
  width: 13,
  length: 54,
});

const chocolat = new Product(458, "chocolat", 15, 51, {
  height: 154,
  width: 45,
  length: 45,
});

let courses = new Order(
  4154,
  lola,
  [chocolat, panierFruits, choucroute, culotte, peanut, crocs],
  new Date()
);

// order.addProduct(chocolat);
// console.log("Adding chocolate to the productList :", order.productsList);

//order.removeProduct(chocolat.productID);
// console.log("Removing chocolate to the productList :", order.productsList);

// console.log(
//   "Tentative de return the big function displayORder",
//   order.displayOrder()
// );
courses.setDelivery(new ExpressDelivery());

// Lancement de la méthode calculateShippingCosts() pour la commande "courses"
console.log(
  "Les frais de livraison s'élèvent à :",
  courses.calculateShippingCosts() + " euros"
);

// Lancement de la méthode estimateDeliveryTime() pour la commande "courses"
console.log(
  "Le temps estimé pour la livraison est de :",
  courses.estimateDeliveryTime() + " days"
);
