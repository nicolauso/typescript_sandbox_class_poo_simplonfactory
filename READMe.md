# Présentation du projet
SimplonFactory est un projet d'apprentissage de la surcouche Typescript. L'application permet de créer des produits, des client.es et des commandes. Les frais et le temps de livraison pourront être calculés pour ces dernières. 

# Installation du projet

## Cloner le repository
## Installer les dépendances
```bash
    npm ci 
``` 

# Lancement
```bash
npm run dev && npm start 
```
 